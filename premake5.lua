---@diagnostic disable: undefined-global, lowercase-global
external_include_dirs = {"/Users/macuser/vcpkg/installed/x64-osx/include/"}
external_lib_dirs = {"/Users/macuser/vcpkg/installed/x64-osx/lib/"}

workspace "gamedev-course"
  configurations { "Debug", "Release" }
  location "build"
  includedirs {
    external_include_dirs
  }
  libdirs {
    external_lib_dirs
  }
  targetdir ("%{wks.location}" .. "/%{cfg.longname}/")
  objdir ("%{wks.location}" .."/%{cfg.longname}/obj/%{prj.name}")

	filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "on"


include "glm-basics"
include "sfml-basics"
