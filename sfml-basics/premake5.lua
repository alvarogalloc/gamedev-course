---@diagnostic disable: undefined-global, lowercase-global
project "sfml-basics"
  kind "ConsoleApp"
  files
  {
    "src/**.cpp",
  }
  defines {"SFML_STATIC"}
  links {
    "sfml-system-s",
    "sfml-network-s",
    "sfml-graphics-s",
    "sfml-window-s",
  }
  filter "system:macosx"
    linkoptions {
      "-mmacosx-version-min=11.6", 
      "-lpthread", 
      "-ObjC",
    }
    links {
      "Foundation.framework",
      "AppKit.framework",
      "IOKit.framework",
      "Carbon.framework",
      "CoreFoundation.framework",
      "CoreAudio.framework",
      "AudioUnit.framework",
      "OpenGL.framework"
    }
