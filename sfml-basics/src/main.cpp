#include <SFML/Graphics.hpp>
#include <climits>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <string>
#include <string_view>

int main(int argc, char *argv[]) {
  sf::Vector2f view_size{1280, 720};

  sf::RenderWindow window{{1280, 720}, "Hello SFML", sf::Style::Close};

  if (argc < 2) {
    std::cerr << "You should set the path to assets\n";
  }
  std::string assets_path = argv[1];

  sf::Texture sky_text;
  if (!sky_text.loadFromFile(assets_path + "/gfx/sky.png")) {
    return 1;
  }
  sf::Sprite sky_sprite;
  sky_sprite.setTexture(sky_text);

  // load all layers of background
  sf::Texture bg_l1;
  if (!bg_l1.loadFromFile(assets_path + "/gfx/bg-1.png")) {
    return 1;
  }
  sf::Sprite bg_sprite_l1;
  bg_sprite_l1.setTexture(bg_l1);
  bg_sprite_l1.setScale(
      {view_size.x / bg_l1.getSize().x, view_size.y / bg_l1.getSize().y});

  sf::Texture bg_l2;
  if (!bg_l2.loadFromFile(assets_path + "/gfx/bg-2.png")) {
    return 1;
  }
  sf::Sprite bg_sprite_l2;
  bg_sprite_l2.setTexture(bg_l2);
  bg_sprite_l2.setScale(
      {view_size.x / bg_l2.getSize().x, view_size.y / bg_l2.getSize().y});

  sf::Texture bg_l3;
  if (!bg_l3.loadFromFile(assets_path + "/gfx/bg-3.png")) {
    return 1;
  }
  sf::Sprite bg_sprite_l3;
  bg_sprite_l3.setTexture(bg_l3);
  bg_sprite_l3.setScale(
      {view_size.x / bg_l3.getSize().x, view_size.y / bg_l3.getSize().y});

  sf::Texture player_text;
  if (!player_text.loadFromFile(assets_path + "/gfx/player.png")) {
    return 1;
  }
  sf::Sprite player_sprite;
  player_sprite.setTexture(player_text);
  player_sprite.setScale({4.f, 4.f});
  player_sprite.setPosition(sf::Vector2f(view_size.x / 2, view_size.y / 2));
  player_sprite.setOrigin(player_text.getSize().x / 2.f,
                          player_text.getSize().y / 2.f);

  while (window.isOpen()) {
    sf::Event ev;
    while (window.pollEvent(ev)) {
      switch (ev.type) {
      case sf::Event::Closed:
        window.close();
        break;
      default:
        break;
      }
    }
    window.clear(sf::Color::White);
    window.draw(sky_sprite);
    window.draw(bg_sprite_l1);
    window.draw(bg_sprite_l2);
    window.draw(bg_sprite_l3);
    window.draw(player_sprite);
    window.display();
  }
  return 0;
}
