#version 430 core
out vec4 Color;

void main() {
  // for each fragment passed, we set the color to blue 
  Color = vec4(0.0f, 0.0f, 1.0f, 1.0f);
}
