#include <glm/glm.hpp>
#ifdef __APPLE__
// Defined before OpenGL and GLUT includes to avoid deprecation messages
#define GL_SILENCE_DEPRECATION
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl3.h>
#endif
// for matrix translations
#include <glm/ext.hpp>

void vertex_specification() {
  /*
    remember: a render pipeline is only
              a series of steps to convert
              3d objects made of vertices to a 2d
              representation as pixels to the screen
    render pipeline steps:
    - Vertex Specification
    - Vertex Shader
    - Vertex Post Processing
    - Primitive Assembly
    - Rasterization
    - Fragment Shader
    - Per Sample Opertation
    - Framebuffer
    note: Steps "Vertex Shader" and "Fragment Shader"
          are modifiable in its process.
  */
  // render pipeline needs VBO's (vertex buffer objects)
  // and VAO's (vertex array objects)
  float vertices[] = {-0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 0.0f};
  uint32_t VAO;
  glGenVertexArrays(1, &VAO);
  uint32_t VBO;
  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  // args:
  // 1. index of attribute.
  // 2. size of each atribute, here our vertex is represented as 3 floats.
  // 3. type of data passed in.
  // 4. boolean whether value should be normalized or not.
  // 5. stride: offset between attributes.
  // 6. starting offset to find first component, casted to a void* type.
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
  // enable attribute at index 0
  glEnableVertexAttribArray(0);

}

int main() {}

void basic_types() {
  // basic types

  // 2d point
  auto p1 = glm::vec2{2.f, 10.f};
  // 3d point
  auto p2 = glm::vec3{10.f, 5.f, 2.f};
  // 4x4 matrix
  auto matrix = glm::mat4{1.f};

  auto translation = glm::translate(matrix, glm::vec3{3.f, 4.f, 8.f});

  auto scale = glm::scale(matrix, glm::vec3{2.f, 2.f, 2.f});

  auto xrotated_matrix =
      glm::rotate(glm::mat4(), glm::radians(45.f), glm::vec3{1, 0, 0});
}
