---@diagnostic disable: undefined-global, lowercase-global
project "glm-basics"
  kind "ConsoleApp"
  files
  {
    "src/**.cpp",
  }
  filter "system:macosx"
    links { "OpenGL.framework" }
